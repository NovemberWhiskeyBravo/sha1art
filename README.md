sha1Art
=============

Why did I made this?
---------------
Inspired by the SHA2017 flag generator I made this generator that generates colours with SHA1.
Why SHA1? Because (SHA1)[https://en.wikipedia.org/wiki/SHA-1] is a hashing algorithm that is being fased out. You can't use it for protection anymore so why not use it for something fun then?
That is what this generator does.

How does it work?
    1. Take input and hash it
    2. Chop the input up and turn it into RGB
    3. Generate the image
    4. ????
    5. Profit
