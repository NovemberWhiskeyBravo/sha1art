import hashlib
import svgwrite
import time
import multiprocessing

colour_array = []

#https://stackoverflow.com/a/36002123
def split_every(n, s):
    return [ s[i:i+n] for i in xrange(0, len(s), n) ]

#https://stackoverflow.com/a/214657
def hex_to_rgb(value):
    value = value.lstrip('#')
    lv = len(value)
    return list(int(value[i:i + lv // 3], 16) for i in range(0, lv, lv // 3))

def to_percentage(input_value):
    output_array = []
    for item in input_value:
        output_array.append(int(float(item)/float(255)*100))
    return output_array

def input_hash(input_to_hash):
    hash_object = hashlib.sha1(input_to_hash)
    hex_dig = hash_object.hexdigest()
    return hex_dig

def date_to_colour():
    current_date_time = time.strftime("%d\%m\%Y-%H:%M:%S")
    current_date_time_encoded = current_date_time.encode('utf-8')
    split_array = split_every(6, input_hash(current_date_time_encoded))
    split_array.pop(6)
    return split_array

def converting_colour0(input_array):
    print "starting converting_colour0"
    temp_array_y = []
    for objects in range(0,len(input_array)):
        temp_array_x = []
        for q in range(0,6):
            hashed_stuff = input_hash(input_array[objects])
            split_array_temp = split_every(6, hashed_stuff)
            split_array_temp.pop(6)
            temp_array_x.append(split_array_temp)
        temp_array_y.append(temp_array_x)
    print "ending converting_colour0"
    return temp_array_y

#this is where the magic happens ;)
def input_to_colour():
    print "starting input_to_colour"
    user_input = open("wanncry.txt","r").read()
    user_input = str(user_input)
    user_input_array = split_every(40,user_input)
    array1 = converting_colour0(user_input_array[:len(user_input_array)/100])
    print "ending input_to_colour"
    return array1

def create_image(name_of_image, colours):
    print "Starting create_image"
    dwg = svgwrite.Drawing('generated/'+name_of_image+'.svg',height='7016px', width='4961px')
    print colours
    for y in range(0,len(colours)):
        for x in range(0,len(colours[y])):
            for x2 in range(0,len(colours[y][x])):
                #print colours[y][x][x2]
                dwg.add(dwg.line(((x*10), y), (((x*10)+1), y), stroke=svgwrite.rgb(colours[y][x][x2][0],colours[y][x][x2][1], colours[y][x][x2][2] , '%')))
    print "ending create_image, saving image"
    dwg.save()
    return

def hash_input():
    print "Starting hash_input"
    colour_list = []
    temp_colour_array = input_to_colour()
    for colours in temp_colour_array:
        elements_y = []
        for colour in colours:
            elements_x = []
            for w in range(0,len(colour)):
                elements_x.append(to_percentage(hex_to_rgb(colour[w])))
            elements_y.append(elements_x)
        colour_list.append(elements_y)
    create_image("hashedWannacry", colour_list)
    print "Ending hash_input"
    return

def hash_date():
    print "Starting hash_date"
    colour_list = []
    start_date_time = time.strftime("%d\%m\%Y-%H:%M:%S")
    end_date_time = time.strftime("%d\%m\%Y-%H:%M:%S")
    for seconds in range(0,60):
        end_date_time = time.strftime("%d\%m\%Y-%H:%M:%S")
        temp_colour_array = date_to_colour()
        temp_colour_list = []
        for colour in temp_colour_array:
            temp_colour_list.append(list(hex_to_rgb(colour)))
        colour_list.append(temp_colour_list)
        print (1+seconds), "second(s)"
        time.sleep(1)
    create_image(start_date_time, colour_list)
    print "Ending hash_date"
    return

#hash_date()
hash_input()
